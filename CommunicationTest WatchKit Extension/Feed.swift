//
//  Feed.swift
//  CommunicationTest WatchKit Extension
//
//  Created by hardeep kaur on 2019-10-31.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import WatchConnectivity

class Feed: WKInterfaceController, WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
    }
    @IBOutlet var hungerLevel: WKInterfaceLabel!
    @IBAction func feedPokemon() {
        print("pressing feed pokemon button to reduce the hunger level of pokemon")
       
      hungerLevel.setText("0")
    }
    
}
