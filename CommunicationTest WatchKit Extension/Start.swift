//
//  Start.swift
//  CommunicationTest WatchKit Extension
//
//  Created by hardeep kaur on 2019-10-31.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import WatchConnectivity

class Start: WKInterfaceController, WCSessionDelegate  {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
    }
    
    
   var counter = 0
    var seconds = 60
    var timer = Timer()
    
    @IBOutlet var timerCount: WKInterfaceTimer!
    @IBAction func startTimer(_ sender : Any)
    {
        print("start timer button pressed")
    
    }

}
